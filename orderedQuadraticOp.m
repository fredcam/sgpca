%%%%%%%%%%%%%%%%
%function to compute various quadratic operators
%for ordered data
%input: n = # of varaibles
%option = the type of operator
%'laplacian'
%'squared 2nd differences'
%'gaussian smoother'
%'epan smoother'
%'gaussian weighted laplacian'
%'epan weighted laplacian'
%param = parametrs for various operators
%output: D = distance matrix
%Q = quadratic operator
%%%%%%%%%%%%%%%%%%%%%%%%%%%


function[Q,D] = orderedQuadraticOp(n,option,param)
D = gallery('fiedler',1:n);
Q = D;
switch option
  case 'laplacian'
    Q = gallery('tridiag',n);
    Q(1,1) = 1; Q(n,n) = 1;
  case 'squared 2nd differences'
    Q = gallery('tridiag',n);
    Q(1,1) = 1; Q(n,n) = 1;
    Q = Q^2;
  case 'gaussian smoother'
    Q = (1/(param*sqrt(2*pi)))*exp(-(D.^(2))./(2*param^2));
    ind = abs(Q)<=1e-4;
    Q(ind) = 0; Q = sparse(Q);
  case 'epan weighted laplacian'
    ind = D>param;
    Q = .75*(1 - (D./param).^2);
    Q(ind) = 0; Q = sparse(Q);
    Q = -Q + 2*diag(diag(Q));
    Q = Q + diag(sum(abs(Q)));
  case 'gaussian weighted laplacian'
    Q = (1/(param*sqrt(2*pi)))*exp(-(D.^(2))./(2*param^2));
    ind = abs(Q)<=1e-4;
    Q(ind) = 0; Q = sparse(Q);
    Q = -Q + 2*diag(diag(Q));
    Q = Q + diag(sum(abs(Q)));
end
Q = Q/max(full(eig(Q)));

