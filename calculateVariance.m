%%%%%%%%%%%%%
%calculates cumulative proprotion of variance


function[var] = calculateVariance(X,Q,R,U,V,K)
	[n,p] = size(X);
	inmat = Q*X*R;
	denomenator = trace(X'*inmat);
	var = zeros(K,1);
	for i = 1:K
		if sum(abs(U(:,i))) ~= 0 && sum(abs(V(:,i))) ~= 0
			Pu = U(:,1:i)*((U(:,1:i)'*Q*U(:,1:i))\U(:,1:i)');
			Pv = V(:,1:i)*((V(:,1:i)'*R*V(:,1:i))\V(:,1:i)');
			Xk = Pu*inmat*Pv;
			numerator = trace(Xk'*Q*Xk*R);
			var(i) = numerator/denomenator;
		elseif i>1
			var(i) = var(i-1);
		end
	end