%%%%%%%%%%%%%%%%%%%%%%%
%function to compute covariance operators from distance matrices
%can be used as generalizing operators to induce smoothness in
%Generalized PCA 
%function call: Q = cov_operators(D,'exp.cov',1);
%inputs:
%D: a symmetric distance matrix (for example, from
%squareform(pdist()) )
%option: type of covariance operator - choices are:
%'exp.cov' - Exponential covariance
%'matern.cov' - Matern class with parameter nu = 2.5
%'cubic.cov' - Cubic covariance
%'spherical.cov' - Spherical covariance
%sigma: parameter for covariance operators
%outputs:
%a symmetric covariance matrix
%these matrices are forced to be positive semi-definite
%also they are scaled to have operator norm 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function[Q] = cov_operators(D,option,sigma)
Q = D;
switch option
  case 'exp.cov'
    Q = exp(-(D.^2)/sigma^2);
  case 'matern.cov'
    Q = (1 + sqrt(5)*D/sigma + 5*D.^2/(3*sigma^2))*exp(-sqrt(5)*D/sigma);
  case 'cubic.cov'
    Q = 1 - (7*D.^2/sigma^2 - 8.75*D.^3/sigma^3 + 3.5*D.^5/sigma^5 - D.^7/sigma^7);
    Q(D>=sigma) = 0;
  case 'spherical.cov'
    Q = 1 - 1.5*D/sigma + .5*D.^3/sigma^3;
    Q(D>=sigma) = 0;
end
[Qvecs Qv] = eig(Q);
Qvals = diag(Qv);
ind = real(Qvals)<=1e-4;
Q = Qvecs(:,~ind)*diag(Qvals(~ind))*Qvecs(:,~ind)'/max(Qvals(~ind));


    
    