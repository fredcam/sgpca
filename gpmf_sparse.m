%%%%%%%%%%%%%%%%%%%
%function to preform GPMF with lasso penalties
%startU and StartV should be n x K and p x K
%pos should be 0 or 1 - 0 = soft thresholding,
%1 = positive soft thresholding


function[U,V,D] = gpmf_sparse(X,Q,R,lamu,lamv,K,startU,startV,posu,posv,maxit)
[n,p] = size(X);
thr = 1e-4;
Xhat = X;
U = []; V = []; D = zeros(K,K);
for i=1:K
    if norm(startU) + norm(startV) == 0
        [u,v] = gmdLA(Xhat,Q,R,1);
    else
        u = startU(:,i); v = startV(:,i);
    end
    err = 1; iter = 0;
    while err > thr & iter<maxit;
        oldu = u; oldv = v;
        u = gpmf_solveuv(sparse(Xhat*R*v),lamu,Q,oldu,posu,maxit); 
        v = gpmf_solveuv(sparse(Xhat'*Q*u),lamv,R,oldv,posv,maxit);
        err = norm(oldu-u,2) + norm(oldv - v,2);
        iter = iter + 1;
    end
    D(i,i) = u'*Q*Xhat*R*v;
    U = [U u]; V = [V v];
    Xhat = Xhat - D(i,i)*u*v';
end


