%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function to compute graphical Laplacians from distance matrices
%inputs:
%D: a symmetric, non-negative distance matrix
%window: a non-negative scalar 
%an edge in the graphical structure will be placed if the two
%variables are within "window" distance apart
%outputs:
%Q - a graphical Laplacian matrix (this is not normalized)
%%%%%%%%%%%%%

function[Q] = dist_laplacian(D,window)
n = size(D,1);
A = (D<=window);
Deg = sum(A);
Q = diag(Deg) - A;




