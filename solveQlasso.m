%%%%%%%%%%%
%Solves the Q weighted lasso problem with
%possible non-negativity constraints


function[u] = solveQlasso(y,lam,Q,startu,pos,maxit);

Qy = Q*y;
thr = 1e-4; L = 1.1;
err = 1; iter = 0;
u = startu;
while err>thr && iter<maxit
    oldu = u;
    u = sparse(soft_thr(oldu + (Qy - Q*oldu)/L,lam/L,pos));
    err = norm(oldu - u)/norm(u);
    iter = iter + 1;
end


