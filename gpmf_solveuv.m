%%%%%%%%%%%%%%%%%%%%%%%
%note: solves for u or v in the gpmf case

function[u] = gpmf_solveuv(y,lam,Q,startu,pos,maxit);
if norm(y)==0
    u = zeros(length(y),1);
elseif lam == 0
    u = y/sqrt(y'*Q*y);
else    
    if sum(sum(Q)) - trace(Q) == 0;
        uhat = sparse(soft_thr(y,(lam./diag(Q)),pos));
        if norm(uhat)==0
            u = zeros(length(y),1);
        else
            u = uhat/sqrt(uhat'*Q*uhat);
        end        
    elseif norm(Q*y)==0
        u = zeros(length(y),1);
    else                
        uhat = solveQlasso(y,lam,Q,startu,pos,maxit);
        if norm(uhat)==0
            u = zeros(length(y),1);
        else
            u = uhat/sqrt(uhat'*Q*uhat);
        end        
    end
end
