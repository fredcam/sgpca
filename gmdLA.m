function[U, V, D, propv,cumv] = gmdLA(X,Q,R,K)
[Rvectors Rval] = eig(full(R));
Rvalues = diag(Rval);
Rvalues(-1e-4<Rvalues & Rvalues<0) = 0;
if min(Rvalues)<0
error('R must be positive semi-definite');
end
ind = real(Rvalues)<=1e-6;
Rtild = Rvectors(:,~ind)*diag(sqrt(Rvalues(~ind)))*Rvectors(:,~ind)';
inmat = X'*Q*X;
[n p] = size(X);
[u,d] = eigs(Rtild*inmat*Rtild,K);
inverse_values = 1./Rvalues(~ind); 
Rtildi = Rvectors(:,~ind) * diag(sqrt(inverse_values)) * Rvectors(:,~ind)';
vgmd = Rtildi*u;
dgmd = sqrt(diag(d));
propv = (dgmd.^2)./trace(inmat*R);
cumv = zeros(K,1);
ugmd = zeros(n,K);
for k=1:K
ugmd(:,k) = X*R*vgmd(:,k)/sqrt(vgmd(:,k)'*R*inmat*R*vgmd(:,k));
cumv(k) = sum(propv(1:k));
end
U = real(ugmd);
V = real(vgmd);
D = real(dgmd);