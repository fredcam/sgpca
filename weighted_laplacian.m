%%%%%%%%%%%%%%%%%%%%%%%%
%function to compute a weighted laplacian using a weight matrix
%inputs:
%W - a symmetric and non-negative weight matrix
%for example - can use - W = cov_operators();
%thres - value at which to threshold weights
%if thres = 0, no thresholding is performed
%outputs:
%Q - a weighted Laplacian matrix (this is not normalized)
%%%%%%%%%%%%%%%%%%%%


function[Q] = weighted_laplacian(W,thres)
A = W - diag(diag(W));
A(W<=thres) = 0;
Deg = sum(A);
Q = diag(Deg) - A;
