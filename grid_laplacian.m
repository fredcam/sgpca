%%%%%%%%%%%%%%%%%%%%%%%
%function to compute graphical Laplacians for data on a grid
%supports data on a 1D, 2D, or 3D grid
%inputs:
%dims: a vector giving the dimensions of the grid
%for example - dims = size(x);
%distop: type of distance to use - options are:
%'cityblock' - (for 2D, this is the 4 nearest neighbor lattice)
%'chebychev' - (for 2D, this connects diagonal neighbors)
%outputs:
%Q - graph laplacian matrix (this is not normalized)
%D - the distance matrix computed
%example call:
%Q = grid_laplacian([10 10],'cityblock');
%Q = Q/eigs(Q,1);
%%%%%%%%%%%%%%%%%%%%%%%

function[Q,D] = grid_laplacian(dims,distop)
switch length(dims)
  case 1
    D = squareform(pdist([1:dims]',distop));
  case 2
    [xx yy] = meshgrid(1:dims(1),1:dims(2));
    D = squareform(pdist([xx(:) yy(:)],distop));
  case 3
    [xx yy zz] = meshgrid(1:dims(1),1:dims(2),1:dims(3));
    D = squareform(pdist([xx(:) yy(:) zz(:)],distop));
end
A = (D==1);
Deg = sum(A);
Q = diag(Deg) - A;
    
