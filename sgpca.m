%%%%%%%%%%%%%%%%%%%
%function to perform Generalized PCA, Sparse GPCA, Two-way Sparse
%GPCA, Sparse Non-negative GPCA, or Two-way Sparse Non-negative GPCA
%inputs: 
%X: n x p data matrix (possibly centered first)
%Q: n x n row quadratic operator
%R: p x p column quadratic operator
%note that Q and R must be positive semi-definite and scaled to
%have operator norm one
%options: a struct containing the following options:
%options.K: the number of factors
%options.lamvs: a vector of column regularization parameters to try
%options.lamu: the row regularization parameter
%options.posu: {0,1}, 1 places non-negativity constraints on the
%row factor
%options.posv: {0,1}, 1 places non-negativity constraints on the
%column factor
%options.convergence: the threshold by which the establish
%convergence
%options.maxit: the maximum number of iterations
%options.full_path: {0,1}, 1 returns the full path of solutions
%computed at each value of options.lamvs given
%note: options.posu (or posv) are only active if both .lamu and
%.lamvs are specified as non-zero.  
%output:
%U: n x K GPCA row factors (unless full_path)
%V: p x K GPCA column factors (unless full_path)
%D: a K vector of GMD values
%bics: a K vector of optimal BIC values computed for each factor
%optlams: a K vector of optimal lamvs values computed for each
%factor
%cumulative_proportion_variance: a K vector indicating the
%cumulative proportion of variance (in the Q,R-norm) explained by
%the factors
%note: if full_path is specified then U and V are returned with
%length(lamvs) x K columns
%example function call: 
%options = {};
%options.K = 2;
%options.lamvs = [.5 1 1.5];
%options.lamu = 0;
%[U,V,D,bics,optlams,cumv] = sgpca(X,Q,R,options);
%%%%%%%%%%%%%%%%%%%%%%%%


function[U,V,D,bics,optlams, cumulative_proportion_variance] = sgpca(X,Q,R,options)


nargchk(3, 4, nargin);

X = double(X);

%% default values

lamvs = 0;
lamu =0;
K = 1;
posu = 0;
posv = 0;
convergence = 1e-6;
maxit = 1000;
full_path = 0;

%% Sanity check inputs


[n,p] = size(X);


[qn, qp] = size(Q);
if qn ~= n
error('Dimensions of Q incorrect');
end

if qp ~= n
error('Dimensions of Q incorrect');
end

[rn, rp] = size(R);
if rn ~= p
error('Dimensions of R incorrect');
end
if rp ~= p
error('Dimensions of R incorrect');
end

if nargin == 4


%% check if the lamvs field is set if it is override default setting   
if isfield(options,'lamvs')   
if ~isnumeric(options.lamvs)   
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field lamvs');   
end
lamvs = options.lamvs;
end


%% check if the lamu field is set if it is override default setting
if isfield(options,'lamu')
if ~isnumeric(options.lamu)
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field lamu');
end
lamu = options.lamu;
end




%% check if the k field is set if it is override default setting
if isfield(options,'K')
if ~isnumeric(options.K)
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field K');
end
K = options.K;
end

%% check if the posu field is set if it is override default setting
if isfield(options,'posu')
if ~isnumeric(options.posu)
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field posu');
end
posu = options.posu;
end


%% check if the posv field is set if it is override default setting
if isfield(options,'posv')
if ~isnumeric(options.posv)
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field posv');
end
posv = options.posv;
end

%% check if the convergence field is set if it is override default setting
if isfield(options,'convergence')
if ~isnumeric(options.convergence)
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field convergence');
end
convergence = options.convergence;
end

%% check if the maxit field is set if it is override default setting
if isfield(options,'maxit')
if ~isnumeric(options.maxit)
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field maxit');
end
maxit = options.maxit;
end


%% check if the full_path option is set if it is override default setting
if isfield(options,'full_path')
if ~isnumeric(options.full_path)
error('sgpca:incorrectInput', ...
	  'sgpca expects a number for the field full_path');
end
full_path  = options.full_path;
end


end



[n,p] = size(X);
r = length(lamvs);
Us = zeros(n,r*K); Vs = zeros(p,r*K); ds = zeros(r*K,1);
bics = zeros(K,1);
Xhat = X; 
row_index = 1;
bic_index = zeros(K,1);
optlams = zeros(K,1);

for i=1:K
    for j=1:r;
	
	%% index of component
	old_index = row_index;
        row_index = (i-1)*r + j;
	
	%% calculate component
	if j == 1
		[startu startv] = gpca(Xhat,Q,R,1);
		[Us(:,row_index),Vs(:,row_index),ds(row_index)] = gpmf_sparse(Xhat,Q,R,lamu,lamvs(j),1,startu,startv,posu,posv,maxit);
        else
		[Us(:,row_index),Vs(:,row_index),ds(row_index)] = gpmf_sparse(Xhat,Q,R,lamu,lamvs(j),1,Us(:,old_index),Vs(:,old_index),posu,posv,maxit);
	end

	%% calculate bic for component
	oldu = Us(:,row_index); oldv = Vs(:,row_index);
        df = sum(oldv~=0);
	temp = Q*(Xhat - ds(j)*oldu*oldv')*R*(Xhat - ds(j)*oldu*oldv')';
        bic = log(trace(temp)/(n*p)) + (log(n*p)/(n*p)).*df;

	%% get minimum bic and index of minimum
	if j == 1
		minbic = bic;
		bic_index(i) = row_index;
		bics(i) = bic;
		optlams(i) = lamvs(j);
	elseif bic < minbic
		minbic = bic;
		bics(i) = bic;
		bic_index(i) = row_index;
		optlams(i) = lamvs(j);
	end		
    end

    %% deflate Xhat
    Xhat = Xhat - ds(bic_index(i))*Us(:,bic_index(i))*Vs(:,bic_index(i))';
end

U = Us(:,bic_index); V = Vs(:,bic_index); D = diag(ds(bic_index));


cumulative_proportion_variance = calculateVariance(X,Q,R,U,V,K);


if full_path
	U = Us; V = Vs; D = ds;
end


    
    
    
    
    
