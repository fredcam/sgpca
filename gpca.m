%%%%%%%%%%%%%
%Generalized PCA function
%inputs:
%X: n x p data matrix (possibly centered first)
%Q: n x n row quadratic operator
%R: p x p column quadratic operator
%note that Q and R must be positive semi-definite and scaled to
%have operator norm one
%K: number of GPCA components to extract
%outputs:
%U: n x K GPCA row factors
%V: p x K GPCA column factors
%D: a K vector of GMD values
%propv: the proportion of variance (in Q,R-norm) explained by the K
%factors
%cumv: the cumulative proportion of variance explained
%example function call: [U,V] = gpca(X,Q,R,2);
%%%%%%%%%%%%%%%%%%%%%

function[U,V,D,propv,cumv] = gpca(X,Q,R,K)
[n,p] = size(X);

if p < n
	[U V D propv cumv] = gmdLA(X,Q,R,K);
else
	[V U D propv cumv] = gmdLA(X',R,Q,K);
end


